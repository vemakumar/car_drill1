// testProblem3.js
const inventory = require("../data/inventory");
const sortCarModelsAlphabetically = require("../problem/problem3");


const sortedCarModels = sortCarModelsAlphabetically(inventory);
console.log(sortedCarModels);

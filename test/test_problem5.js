// testProblem5.js
const inventory = require("../data/inventory");
const countCarsOlderThanYear = require("../problem/problem5");

const year = 2000;
const olderCarsCount = countCarsOlderThanYear(inventory, year);
console.log(`Number of cars older than ${year}: ${olderCarsCount}`);

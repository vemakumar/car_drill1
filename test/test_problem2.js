// testProblem2.js
const inventory = require("../data/inventory");
const getLastCar = require("../problem/problem2");


const lastCar = getLastCar(inventory);
if (lastCar) {
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
} else {
    console.log('No cars in inventory.');
}

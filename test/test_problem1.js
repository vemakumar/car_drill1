// testProblem1.js

const inventory = require("../data/inventory");

const findCarById = require("../problem/problem1");

const carId = 33;

const foundCar = findCarById(inventory, carId);
if (foundCar) {
    console.log(`Car ${carId} is a ${foundCar.car_year} ${foundCar.car_make} ${foundCar.car_model}`);
} else {
    console.log(`Car with ID ${carId} not found.`);
}

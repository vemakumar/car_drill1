// problem2.js
function getLastCar(inventory) {
    if (inventory.length === 0) {
        return null; // No cars in inventory
    }
    return inventory[inventory.length - 1];
}

module.exports = getLastCar;

